#[macro_use]
extern crate assert_matches;

extern crate rdiff;
use rdiff::types::*;
use rdiff::*;

use std::io::Read; // for str -> &[u8] -> io::Bytes conversion

fn get_signatures(s: &str, chunk_size: usize) -> SignatureList {
    let mut data = s.as_bytes().bytes();
    let gen = chunk_gen::ChunkGenerator::init(chunk_size, &mut data);
    let chunks: Result<Vec<_>, _> = gen.collect();
    SignatureList {
        chunk_size: chunk_size as u64,
        chunks: chunks.unwrap(),
    }
}

fn get_deltas(sig: &SignatureList, s: &str) -> Vec<Delta> {
    let mut data = s.as_bytes().bytes();
    let gen = delta_gen::DeltaGenerator::init(&sig, &mut data);
    let deltas: Result<Vec<_>, _> = gen.collect();
    deltas.unwrap()
}

#[test]
#[rustfmt::skip::macros(assert_matches)]
fn test_delta_change_start_aligned() {
    let sig = get_signatures("abcdefgh", 4);
    let ds = get_deltas(&sig, "Gbcdefgh");
    assert_eq!(ds.len(), 2);
    assert_matches!(&ds[0], Delta::Insertion { data } if data.len() == 4);
    assert_matches!(&ds[1], Delta::Match {old_start: 4, new_start: 4});
}

#[test]
#[rustfmt::skip::macros(assert_matches)]
fn test_delta_change_start_unaligned() {
    // the last chunk is zero-padded
    let sig = get_signatures("abcdefghijk", 4);
    let ds = get_deltas(&sig, "Gbcdefghijk");
    assert_eq!(ds.len(), 3);
    assert_matches!(&ds[0], Delta::Insertion { data } if data.len() == 4);
    assert_matches!(&ds[1], Delta::Match {old_start: 4, new_start: 4});
    assert_matches!(&ds[2], Delta::Match {old_start: 8, new_start: 8});
}

#[test]
#[rustfmt::skip::macros(assert_matches)]
fn test_delta_insertion_middle_aligned() {
    let sig = get_signatures("abcdefgh", 4);
    let ds = get_deltas(&sig, "abcdXYZefgh"); // inserted 3 bytes
    assert_eq!(ds.len(), 3);
    assert_matches!(&ds[0], Delta::Match {old_start: 0, new_start: 0});
    assert_matches!(&ds[1], Delta::Insertion { data } if data.len() == 3);
    assert_matches!(&ds[2], Delta::Match {old_start: 4, new_start: 7});
}

#[test]
#[rustfmt::skip::macros(assert_matches)]
fn test_delta_insertion_middle_unaligned() {
    // the insertion breaks up the surrounding chunks
    let sig = get_signatures("abcdefgh", 4);
    let ds = get_deltas(&sig, "abcXYZdefgh"); // inserted 3 bytes
    assert_eq!(ds.len(), 3);
    // first insertion is chunk size in length ("abcX")
    assert_matches!(&ds[0], Delta::Insertion { data } if data.len() == 4);
    // the next one is only 3 bytes long ("YZd") because it's before a match
    assert_matches!(&ds[1], Delta::Insertion { data } if data.len() == 3);
    assert_matches!(&ds[2], Delta::Match {old_start: 4, new_start: 7});
}

#[test]
#[rustfmt::skip::macros(assert_matches)]
fn test_delta_insertion_both_sides() {
    let sig = get_signatures("abcd", 4);
    let ds = get_deltas(&sig, "xyzwtabcdefg");
    assert_eq!(ds.len(), 4);
    assert_matches!(&ds[0], Delta::Insertion { data } if data.len() == 4);
    assert_matches!(&ds[1], Delta::Insertion { data } if data.len() == 1);
    assert_matches!(&ds[2], Delta::Match {old_start: 0, new_start: 5});
    assert_matches!(&ds[3], Delta::Insertion { data } if data.len() == 3);
}

#[test]
#[rustfmt::skip::macros(assert_matches)]
fn test_delta_deletion_middle_aligned() {
    let sig = get_signatures("abcdefghijkl", 4);
    let ds = get_deltas(&sig, "abcdijkl"); // removed 4 bytes
    assert_eq!(ds.len(), 2);
    assert_matches!(&ds[0], Delta::Match {old_start: 0, new_start: 0});
    assert_matches!(&ds[1], Delta::Match {old_start: 8, new_start: 4});
}

#[test]
#[rustfmt::skip::macros(assert_matches)]
fn test_delta_deletion_middle_unaligned() {
    let sig = get_signatures("abcdefghijkl", 4);
    let ds = get_deltas(&sig, "abchijkl"); // removed 4 bytes
    assert_eq!(ds.len(), 2);
    assert_matches!(&ds[0], Delta::Insertion { data } if data.len() == 4);
    assert_matches!(&ds[1], Delta::Match {old_start: 8, new_start: 4});
}

#[test]
#[rustfmt::skip::macros(assert_matches)]
fn test_delta_deletion_end_aligned() {
    let sig = get_signatures("abcdefghijkl", 4);
    let ds = get_deltas(&sig, "abcdefgh");
    assert_eq!(ds.len(), 2);
    assert_matches!(&ds[0], Delta::Match {old_start: 0, new_start: 0});
    assert_matches!(&ds[1], Delta::Match {old_start: 4, new_start: 4});
}

#[test]
#[rustfmt::skip::macros(assert_matches)]
fn test_delta_deletion_end_unaligned() {
    let sig = get_signatures("abcdefghijkl", 4);
    let ds = get_deltas(&sig, "abcdefg");
    assert_eq!(ds.len(), 2);
    assert_matches!(&ds[0], Delta::Match {old_start: 0, new_start: 0});
    assert_matches!(&ds[1], Delta::Insertion { data } if data.len() == 3);
}

#[test]
#[rustfmt::skip::macros(assert_matches)]
fn test_delta_duplicate_detection() {
    let sig = get_signatures("abcdefgh", 4);
    let ds = get_deltas(&sig, "abcdefghabcdefgh");
    assert_eq!(ds.len(), 4);
    assert_matches!(&ds[0], Delta::Match {old_start: 0, new_start: 0});
    assert_matches!(&ds[1], Delta::Match {old_start: 4, new_start: 4});
    assert_matches!(&ds[2], Delta::Match {old_start: 0, new_start: 8});
    assert_matches!(&ds[3], Delta::Match {old_start: 4, new_start: 12});
}
