RDiff algorithm in Rust.

Where to find various pieces:

- src/types.rs - includes Rabin-Karp fingerprint calculation
- src/chunk_gen.rs - signature generation
- src/delta_gen.rs - delta generation
- tests/delta.rs - end-to-end tests (i.e. comparing two inputs)

Both ChunkGenerator and DeltaGenerator are implemented as iterators.
They consume std::io::Bytes<R> as input and yield chunks/deltas as output.



Example of usage:

pushd /tmp
wget -nc https://tools.ietf.org/id/draft-ietf-ace-mqtt-tls-profile-06.txt
wget -nc https://tools.ietf.org/id/draft-ietf-ace-mqtt-tls-profile-07.txt
popd

cargo build

./target/debug/rdiff signature /tmp/draft-ietf-ace-mqtt-tls-profile-06.txt sig-06.cbor
./target/debug/rdiff delta sig-06.cbor /tmp/draft-ietf-ace-mqtt-tls-profile-07.txt delta-07.cbor
./target/debug/rdiff patch /tmp/draft-ietf-ace-mqtt-tls-profile-06.txt delta-07.cbor result-07.txt

vimdiff result-07.txt /tmp/draft-ietf-ace-mqtt-tls-profile-07.txt




Copyright (C) 2020  Artem Tarasov
Licensed under WTFPL.
