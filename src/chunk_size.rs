// total memory consumption is comprised of:
// - current chunk contents (proportional to chunk size)
// - signatures (proportional to the inverse of chunk size)
fn ram_estimate(input_size: u64, chunk_size: f64) -> f64 {
    // very rough estimate of memory required to store hashes
    // for a single block (aka Chunk), including all sorts of
    // overheads, such as from use of hash tables for access
    let record_size: f64 = 100.0;

    // the factor of 4 is fairly lax and assumes sane amount
    // of input data copying / buffering to occur
    let num_chunks = input_size as f64 / chunk_size;
    4.0 * chunk_size + record_size * num_chunks.ceil()
}

// chunk size grows as square root of input size;
// this allows the two components to have similar magnitude
pub fn recommend_chunk_size(input_size: u64) -> usize {
    // let's target ~10MB for signatures on a 1TB input;
    // if each record is ~100B (including some overheads),
    // for such input there'd be 10^7/100 = 10^5 chunks;
    // thus, a good constant is 10^5 / sqrt(10^12) = 0.1
    let factor: f64 = 0.1;
    let num_chunks: f64 = (input_size as f64).sqrt() * factor;
    let chunk_size: f64 = (input_size as f64) / num_chunks;

    let size_step: f64 = 64.0;
    let mut n = (chunk_size / size_step).ceil();

    // if there's spare memory, let's reduce the chunk size
    let min_ram: f64 = 1.33e7;
    while ram_estimate(input_size, n * size_step) < min_ram {
        if n <= 1.0 {
            break;
        }
        n /= 1.5;
    }
    n.ceil() as usize * size_step as usize
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_chunk_size() {
        assert_eq!(recommend_chunk_size(1), 64);
        assert_eq!(recommend_chunk_size(10), 64);
        assert_eq!(recommend_chunk_size(100), 64);

        assert_eq!(recommend_chunk_size(1e6 as u64), 64);
        assert_eq!(recommend_chunk_size(1e7 as u64), 128);
        assert_eq!(recommend_chunk_size(1e8 as u64), 576);
        assert_eq!(recommend_chunk_size(1e9 as u64), 5504);
        assert_eq!(recommend_chunk_size(1e10 as u64), 58560);
        assert_eq!(recommend_chunk_size(1e11 as u64), 3162304);
        assert_eq!(recommend_chunk_size(1e12 as u64), 10000000);
    }
}
