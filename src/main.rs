use clap::Clap;

use std::fs;
use std::io::{Read, Seek, SeekFrom, Write};

use rdiff::types::*;
use rdiff::*;

#[derive(Clap)]
#[clap(version = "0.1", author = "Artem T. <artem@devopps.de>")]
struct Opts {
    #[clap(subcommand)]
    subcmd: SubCommand,
}

#[derive(Clap)]
enum SubCommand {
    Signature(Signature),
    Delta(Delta),
    Patch(Patch),
}

#[derive(Clap)]
struct Signature {
    old_file: String,
    signature_file: String,
}

#[derive(Clap)]
struct Delta {
    signature_file: String,
    new_file: String,
    delta_file: String,
}

#[derive(Clap)]
struct Patch {
    old_file: String,
    delta_file: String,
    new_file: String,
}

#[cfg(not(tarpaulin_include))]
fn file_signatures(
    filename: &str,
    chunk_size: Option<usize>,
) -> Result<SignatureList, std::io::Error> {
    let input_size = fs::metadata(filename)?.len();
    let default_chunk_size = || chunk_size::recommend_chunk_size(input_size);
    let chunk_size = chunk_size.unwrap_or_else(default_chunk_size);
    println!("using chunk size {} for file {}", chunk_size, filename);

    let input_file = fs::File::open(filename)?;
    let mut data = input_file.bytes();

    let gen = chunk_gen::ChunkGenerator::init(chunk_size, &mut data);
    let chunks: Result<Vec<_>, _> = gen.collect();

    Ok(SignatureList {
        chunk_size: chunk_size as u64,
        chunks: chunks?,
    })
}

#[cfg(not(tarpaulin_include))]
fn main() -> Result<(), Box<dyn std::error::Error>> {
    // this is enough for a PoC but nowhere near being production-ready
    // TODO avoid .collect() calls, use iterators instead
    // TODO human-friendly error handling
    // TODO use BufReader throughout

    let opts: Opts = Opts::parse();

    match opts.subcmd {
        SubCommand::Signature(s) => {
            let sig = file_signatures(&s.old_file, None)?;
            let out_file = fs::File::create(&s.signature_file)?;
            let writer = snap::write::FrameEncoder::new(out_file);
            serde_cbor::to_writer(writer, &sig)?;
            Ok(())
        }

        SubCommand::Delta(d) => {
            let sig_file = fs::File::open(&d.signature_file)?;
            let reader = snap::read::FrameDecoder::new(sig_file);
            let sig: SignatureList = serde_cbor::from_reader(reader)?;

            let input_file = fs::File::open(&d.new_file)?;
            let mut data = input_file.bytes();

            let gen = delta_gen::DeltaGenerator::init(&sig, &mut data);
            let deltas: Result<Vec<_>, _> = gen.collect();

            let out_file = fs::File::create(&d.delta_file)?;
            let writer = snap::write::FrameEncoder::new(out_file);
            let delta_list = DeltaList {
                chunk_size: sig.chunk_size,
                deltas: deltas?,
            };
            serde_cbor::to_writer(writer, &delta_list)?;
            Ok(())
        }

        SubCommand::Patch(p) => {
            let mut old_file = fs::File::open(&p.old_file)?;
            let mut new_file = fs::File::create(&p.new_file)?;

            let delta_file = fs::File::open(&p.delta_file)?;
            let reader = snap::read::FrameDecoder::new(delta_file);
            let delta_list: DeltaList = serde_cbor::from_reader(reader)?;
            let chunk_size = delta_list.chunk_size as usize;
            let mut buffer: Vec<u8> = vec![0; chunk_size];

            for delta in delta_list.deltas.iter() {
                match delta {
                    rdiff::types::Delta::Match { old_start, .. } => {
                        old_file.seek(SeekFrom::Start(*old_start))?;
                        let bytes_read = old_file.read(&mut buffer[..])?;
                        new_file.write_all(&buffer[..bytes_read])?;
                    }
                    rdiff::types::Delta::Insertion { data } => {
                        new_file.write_all(&data[..])?;
                    }
                }
            }

            Ok(())
        }
    }
}
