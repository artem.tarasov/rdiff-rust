use crate::types::*;

use std::collections::{BTreeMap, VecDeque};
use std::io;

// Implementation notes:
// - the last <chunk_size> bytes of the deque is the current window;
// - we maintain queue size > chunk_size except for the beginning,
//   in order to access the old byte in rolling hash calculation;
// - as soon as the queue contains 2 * <chunk_size> bytes, we drain
//   the first <chunk_size> bytes to avoid complications in tracking
//   the matches when the queue contains two chunks simultaneously
//
// for each input byte:
// - add the byte to the deque;
// - if the deque size is 2 * <chunk_size>:
//   - if chunk_match is Some(x), yield x and reset chunk_match to None
//   - otherwise, yield first <chunk_size> bytes as a change;
//     no new window can include them as they are too far away;
//   - either way, drain first <chunk_size> bytes from the queue;
// - if last <chunk_size> bytes match both rolling and secure hashes:
//   - yield contents prior to the current window as a change;
//   - drain the queue leaving only the last <chunk_size> bytes;
// - finally, there's a twist when we reach the end of input;
//   we need to look at the last chunk which might be padded with 0s,
//   and use its original length when checking for a match

struct ChunkMatch<'a> {
    orig_chunk: &'a Chunk,
    new_start: u64,
}

pub struct DeltaGenerator<'a, R> {
    // original chunk signatures
    sig: &'a SignatureList,

    // new data
    input_bytes: &'a mut io::Bytes<R>,

    // because rolling hashes might collide, we need a multimap;
    // an easy way to have it is to use (rolling hash, start pos) as keys,
    // so that the start position guarantees uniqueness
    signature_map: BTreeMap<(RollingHash, u64), &'a Chunk>,

    // (2 * chunk_size)-wide sliding window
    buf: VecDeque<u8>,

    // current position in the new data
    pos: usize,

    // current match (buf front)
    chunk_match: Option<ChunkMatch<'a>>,

    // factor for rolling hash calculation
    byte_out_factor: u64,

    // rolling hash of the last <chunk_size> bytes
    hash: Option<RollingHash>,
}

impl<'a, R> DeltaGenerator<'a, R>
where
    R: io::Read,
{
    pub fn init(sig: &'a SignatureList, data: &'a mut io::Bytes<R>) -> DeltaGenerator<'a, R> {
        let mut signature_map: BTreeMap<(RollingHash, u64), &Chunk> = BTreeMap::new();

        for (i, chunk) in sig.chunks.iter().enumerate() {
            let key = (chunk.rolling_hash, chunk.start);
            signature_map.insert(key, &sig.chunks[i]);
        }

        DeltaGenerator {
            sig,
            input_bytes: data,
            signature_map,
            buf: VecDeque::with_capacity(2 * sig.chunk_size as usize),
            pos: 0,
            chunk_match: None,
            byte_out_factor: RollingHash::byte_out_factor(sig.chunk_size as usize),
            hash: None,
        }
    }

    fn chunk_size(&self) -> usize {
        self.sig.chunk_size as usize
    }

    fn max_queue_size(&self) -> usize {
        2 * self.chunk_size()
    }

    // updates the rolling hash; must be called after a byte has been appended,
    // but before the front of the queue is drained - to access the byte_out
    fn roll(&mut self, byte_in: u8) {
        if self.hash.is_none() && self.buf.len() == self.chunk_size() {
            // initialize the rolling hash
            let contiguous_buf: Vec<_> = self.buf.iter().copied().collect();
            self.hash = Some(RollingHash::compute(&contiguous_buf[..]))
        } else if self.hash.is_some() {
            // update the rolling hash (while we haven't drained the queue)
            let byte_out = self.buf[self.buf.len() - 1 - self.chunk_size()];
            let hash = self.hash.unwrap();
            self.hash = Some(hash.roll(byte_in, byte_out, self.byte_out_factor));
        }

        self.pos += 1;
    }

    // attempts to find a chunk matching the back of the queue
    fn back_match(&self) -> Option<ChunkMatch<'a>> {
        if let Some(rh) = self.hash {
            for (_, &chunk) in self.signature_map.range((rh, 0)..(rh, u64::MAX)) {
                // TODO use buf.as_slices() and blake3::Hasher::update to avoid allocations;
                // or at least reuse the vector across calls
                let contiguous_buf: Vec<_> = self.buf.iter().copied().collect();
                let chunk_data = &contiguous_buf[self.buf.len() - self.chunk_size()..];
                let secure_hash = SecureHash::compute(&chunk_data);

                if chunk.secure_hash == secure_hash {
                    return Some(ChunkMatch {
                        orig_chunk: chunk,
                        new_start: (self.pos - self.chunk_size()) as u64,
                    });
                }
            }
        }

        None
    }

    // removes self.chunk_size() bytes from the front
    fn drain_front(&mut self) -> Delta {
        let delta = if let Some(m) = &self.chunk_match {
            self.buf.drain(0..self.chunk_size());
            Delta::Match {
                old_start: m.orig_chunk.start,
                new_start: m.new_start,
            }
        } else {
            let iter = self.buf.drain(0..self.chunk_size());
            Delta::Insertion {
                data: iter.collect(),
            }
        };

        self.chunk_match = None;

        delta
    }

    fn drain_insertion_before_match(&mut self) -> Delta {
        let len = self.buf.len() - self.chunk_size();
        Delta::Insertion {
            data: self.buf.drain(..len).collect(),
        }
    }

    // drains the queue once all input has been consumed
    fn flush(&mut self) -> Option<Delta> {
        if self.buf.is_empty() {
            return None;
        } else if self.chunk_match.is_some() {
            return Some(self.drain_front());
        }

        let last_chunk = self.sig.chunks.last().unwrap();
        let last_len = (last_chunk.end - last_chunk.start) as usize;

        if self.buf.len() < last_len {
            return Some(Delta::Insertion {
                data: self.buf.drain(..).collect(),
            });
        }

        // pad the buffer with zeroes and recompute the rolling hash
        let num_zeroes = self.chunk_size() - last_len;
        self.pos += num_zeroes;
        for _ in 0..num_zeroes {
            self.buf.push_back(0);
        }
        let old_hash = self.hash;
        let padded_input: Vec<_> = self.buf.iter().copied().collect();
        let offset = self.buf.len() - self.chunk_size();
        self.hash = Some(RollingHash::compute(&padded_input[offset..]));

        // is there a match now?
        self.chunk_match = self.back_match();
        if self.chunk_match.is_some() {
            if self.chunk_size() < self.buf.len() {
                // the match itself will be returned in the next iteration
                return Some(self.drain_insertion_before_match());
            } else {
                // chunk_size > buf.len() is ruled out by a previous check
                return Some(self.drain_front());
            }
        }

        // if we landed here, just roll everything back
        self.pos -= num_zeroes;
        self.buf.drain(self.buf.len() - num_zeroes..);
        self.hash = old_hash;

        // two iterations will be needed if buf.len() > chunk_size
        let len = self.chunk_size().min(self.buf.len());
        Some(Delta::Insertion {
            data: self.buf.drain(..len).collect(),
        })
    }
}

impl<R> Iterator for DeltaGenerator<'_, R>
where
    R: io::Read,
{
    type Item = Result<Delta, std::io::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.input_bytes.next() {
                Some(Ok(byte)) => {
                    self.buf.push_back(byte);
                    self.roll(byte);
                }
                Some(Err(e)) => return Some(Err(e)),
                None => return self.flush().map(Result::Ok),
            }

            if self.buf.len() == self.max_queue_size() {
                let delta = self.drain_front();
                self.chunk_match = self.back_match();
                return Some(Ok(delta));
            }

            if self.chunk_match.is_some() {
                // the queue front already matches something => ignore overlaps
                continue;
            }

            self.chunk_match = self.back_match();

            if self.chunk_match.is_some() && self.chunk_size() < self.buf.len() {
                return Some(Ok(self.drain_insertion_before_match()));
            }
        }
    }
}
