use serde_derive::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub struct SecureHash(pub [u8; 32]);

impl SecureHash {
    pub fn compute(buffer: &[u8]) -> SecureHash {
        SecureHash(*blake3::hash(&buffer[..]).as_bytes())
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Chunk {
    pub start: u64,
    pub end: u64,
    pub rolling_hash: RollingHash,
    pub secure_hash: SecureHash,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SignatureList {
    pub chunk_size: u64,
    pub chunks: Vec<Chunk>,
}

#[derive(Debug, Deserialize, Serialize)]
pub enum Delta {
    Match { old_start: u64, new_start: u64 },
    Insertion { data: Vec<u8> },
}

#[derive(Debug, Deserialize, Serialize)]
pub struct DeltaList {
    pub chunk_size: u64,
    pub deltas: Vec<Delta>,
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Copy, Clone, Deserialize, Serialize)]
pub struct RollingHash(pub u64);

const ROLLING_HASH_PRIME: u64 = (1 << 31) - 1;
const ROLLING_HASH_FACTOR: u64 = 1 << 8;

fn powermod(a: u64, n: u64, p: u64) -> u64 {
    if n == 0 {
        return 1;
    }

    let m = powermod(a, n / 2, p) as u128;
    let m_squared = (m * m) % (p as u128);
    if n % 2 == 1 {
        ((m_squared * a as u128) % (p as u128)) as u64
    } else {
        m_squared as u64
    }
}

impl RollingHash {
    pub fn compute(buffer: &[u8]) -> RollingHash {
        let mut hash: u64 = 0;
        buffer.iter().for_each(|b| {
            hash = hash * ROLLING_HASH_FACTOR + (*b as u64);
            hash %= ROLLING_HASH_PRIME;
        });
        RollingHash(hash)
    }

    // the caller must pass the result in .roll() calls
    pub fn byte_out_factor(chunk_size: usize) -> u64 {
        powermod(
            ROLLING_HASH_FACTOR,
            (chunk_size - 1) as u64,
            ROLLING_HASH_PRIME,
        )
    }

    // the onus is on the caller to keep track of the byte to remove
    pub fn roll(self, byte_in: u8, byte_out: u8, byte_out_factor: u64) -> RollingHash {
        let hash = self.0 + (ROLLING_HASH_PRIME << 8) - (byte_out as u64) * byte_out_factor;
        let hash = hash * ROLLING_HASH_FACTOR + byte_in as u64;
        RollingHash(hash % ROLLING_HASH_PRIME)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_powermod() {
        assert_eq!(powermod(2, 5, 7), 4);
        assert_eq!(powermod(3, 4, 10), 1);
        assert_eq!(powermod(5, 0, 17), 1);
        assert_eq!(powermod(7, 3, 297), 46);
    }

    #[test]
    fn test_rolling_hash() {
        let data: [u8; 7] = [0, 1, 2, 3, 4, 5, 6];
        let factor = RollingHash::byte_out_factor(4);

        let rh = RollingHash::compute(&data[..4]);
        let rh2 = rh.roll(data[4], data[0], factor);
        let rh3 = rh2.roll(data[5], data[1], factor);
        let rh4 = rh3.roll(data[6], data[2], factor);
        assert_eq!(rh2, RollingHash::compute(&data[1..5]));
        assert_eq!(rh3, RollingHash::compute(&data[2..6]));
        assert_eq!(rh4, RollingHash::compute(&data[3..7]));
    }
}
