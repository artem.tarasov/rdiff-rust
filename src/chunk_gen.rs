use crate::types::*;

use std::io;

pub struct ChunkGenerator<'a, R>
where
    R: io::Read,
{
    chunk_size: usize,
    input_bytes: &'a mut io::Bytes<R>,

    buffer: Vec<u8>,
    eof: bool,
    curr: u64,
}

impl<R> ChunkGenerator<'_, R>
where
    R: io::Read,
{
    pub fn init(chunk_size: usize, data: &mut io::Bytes<R>) -> ChunkGenerator<R> {
        ChunkGenerator {
            chunk_size,
            input_bytes: data,

            buffer: vec![0; chunk_size],
            eof: false,
            curr: 0,
        }
    }
}

impl<R> Iterator for ChunkGenerator<'_, R>
where
    R: io::Read,
{
    type Item = Result<Chunk, std::io::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.eof {
            return None;
        }

        // consume <chunk_size> bytes, padding with zeros if necessary
        let mut end = self.curr;
        for i in 0..self.chunk_size {
            self.buffer[i] = match self.input_bytes.next() {
                Some(Ok(b)) => {
                    end += 1;
                    b
                }
                Some(Err(err)) => return Some(Err(err)),
                None => {
                    self.eof = true;
                    0
                }
            }
        }

        let start = self.curr;
        self.curr = end;

        if end > start {
            Some(Ok(Chunk {
                start,
                end,
                rolling_hash: RollingHash::compute(&self.buffer[..]),
                secure_hash: SecureHash::compute(&self.buffer[..]),
            }))
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_chunk_gen() {
        use std::io::Read;

        // input length > chunk size, last chunk is not full
        let data: Vec<u8> = vec![0; 500];
        let mut bytes = data.bytes();
        let gen = ChunkGenerator::init(64, &mut bytes);
        let chunks: Vec<_> = gen.filter_map(Result::ok).collect();
        assert_eq!(chunks.len(), 8);
        assert_eq!(chunks.first().unwrap().start, 0);
        assert_eq!(chunks.first().unwrap().end, 64);
        assert_eq!(chunks.last().unwrap().start, 512 - 64);
        assert_eq!(chunks.last().unwrap().end, 500);

        // input length > chunk size, last chunk is full
        let data: Vec<u8> = vec![0; 500];
        let mut bytes = data.bytes();
        let gen = ChunkGenerator::init(50, &mut bytes);
        let chunks: Vec<_> = gen.filter_map(Result::ok).collect();
        assert_eq!(chunks.len(), 10);
        assert_eq!(chunks.last().unwrap().end, 500);

        // input length < chunk size
        let mut bytes = data.bytes();
        let gen = ChunkGenerator::init(512, &mut bytes);
        let chunks: Vec<_> = gen.filter_map(Result::ok).collect();
        assert_eq!(chunks.len(), 1);
        assert_eq!(chunks.first().unwrap().start, 0);
        assert_eq!(chunks.first().unwrap().end, 500);

        // empty input must result in empty output
        let mut bytes = data[0..0].bytes();
        let gen = ChunkGenerator::init(512, &mut bytes);
        let chunks: Vec<_> = gen.filter_map(Result::ok).collect();
        assert_eq!(chunks.len(), 0);
    }
}
